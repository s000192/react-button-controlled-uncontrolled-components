module.exports = {
  // [...]
  // Replace `ts-jest` with the preset you want to use
  // from the above list
  preset: 'ts-jest',
  "transform": {
    "^.+\\.js$": "<rootDir>/node_modules/ts-jest/preprocessor.js"
  },
  moduleNameMapper: {
    '\\.(css|less)$': '/Users/CALVIN/Test (React)/src/__test__/__mocks__/styleMock.js',
  },
  "resolver": undefined
};