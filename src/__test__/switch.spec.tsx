import * as TestRenderer from "react-test-renderer";
import * as React from "react";
import ReactDOM from "react-dom";
import Switch /* , SwitchProps, SwitchState */ from "../Switch";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Switch disabled />, div);
    ReactDOM.unmountComponentAtNode(div);
});
