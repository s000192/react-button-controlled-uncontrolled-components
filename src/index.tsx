import React from "react";
import ReactDOM from "react-dom";
import Switch from "./Switch";

interface State {
    controlledSwitchChecked: boolean;
    controlledSwitchDisabled: boolean;
    uncontrolledSwitchDisabled: boolean;
}

class App extends React.PureComponent<{}, State> {
    public state: State = {
        controlledSwitchChecked: false,
        controlledSwitchDisabled: false,
        uncontrolledSwitchDisabled: false,
    };

    controlledToggleDisable = () =>
        this.setState({
            controlledSwitchDisabled: !this.state.controlledSwitchDisabled,
        });

    uncontrolledToggleDisable = () =>
        this.setState({
            uncontrolledSwitchDisabled: !this.state.uncontrolledSwitchDisabled,
        });

    controlledOnChange = (checked: boolean) => this.setState({controlledSwitchChecked: checked});

    switchRef = React.createRef<Switch>();

    render() {
        return (
            <React.Fragment>
                <p>Uncontrolled:</p>
                <div>
                    <Switch key="uncontrolled-switch" ref={this.switchRef} disabled={this.state.uncontrolledSwitchDisabled} />
                    <button key="uncontrolled-disabled" type="button" onClick={this.uncontrolledToggleDisable}>
                        Toggle Disable
                    </button>
                </div>
                <p>Controlled:</p>
                <div>
                    <Switch key="controlled-switch" checked={this.state.controlledSwitchChecked} disabled={this.state.controlledSwitchDisabled} onChange={this.controlledOnChange} />
                    <button key="controlled-disabled" type="button" onClick={this.controlledToggleDisable}>
                        Toggle Disable
                    </button>
                </div>
            </React.Fragment>
        );
    }
}

ReactDOM.render(<App />, document.getElementById("app")!);
