import React from "react";
import "./index.css";

interface SwitchProps {
    checked?: boolean | null;
    onChange?: (checked: boolean) => void;
    disabled: boolean;
}

interface SwitchState {
    checked: boolean;
    justLoaded: boolean;
}

export default class Switch extends React.PureComponent<SwitchProps, SwitchState> {
    constructor(props: SwitchProps) {
        super(props);
        this.state = {
            checked: props.checked ? props.checked : false,
            justLoaded: true,
        };
    }

    get checked() {
        return this.state.checked;
    }

    static getDerivedStateFromProps(nextProps: SwitchProps, currentState: SwitchState) {
        if (nextProps.hasOwnProperty("checked") && nextProps.checked !== currentState.checked) {
            return {
                checked: nextProps.checked,
            };
        }

        return null;
    }

    onChange = () => {
        const {disabled, onChange} = this.props;
        const {checked: checkedInState} = this.state;
        this.setState({
            justLoaded: false,
        });
        if (!disabled) {
            if (!this.props.hasOwnProperty("checked")) {
                this.setState({
                    justLoaded: false,
                    checked: !checkedInState,
                });
            }
            if (onChange) {
                onChange(!checkedInState);
            }
        }
    };

    render() {
        const {checked, justLoaded} = this.state;

        return (
            <div>
                <button style={justLoaded ? {animation: "none"} : {}} className={checked ? "toggle-button checked-button" : "toggle-button unchecked-button"} onClick={this.onChange} />
                <div style={justLoaded ? {animation: "none"} : {}} className={checked ? "comp-switch checked-background" : "comp-switch"} />
            </div>
        );
    }
}
